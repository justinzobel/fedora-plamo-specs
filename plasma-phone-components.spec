# org.kde.phone.homescreen and org.kde.plasma.phone.taskpanel are internal,
%global __requires_exclude qmlimport\\((org\\.kde\\.phone\\.homescreen|org\\.kde\\.plasma\\.phone\\.taskpanel|MeeGo\\.QOfono).*

%define kf5_version 5.89.0

Name:           plasma-phone-components
Version:        5.23.4
Release:        1%{?dist}
# Full Plasma 5 version (e.g. 5.9.3)
%{!?_plasma5_bugfix: %define _plasma5_bugfix %{version}}
# Latest ABI-stable Plasma (e.g. 5.8 in KF5, but 5.9.3 in KUF)
%{!?_plasma5_version: %define _plasma5_version %(echo %{_plasma5_bugfix} | awk -F. '{print $1"."$2}')}
Summary:        Plasma Mobile
License:        GPL-2.0-or-later
Group:          System/GUI/KDE
URL:            http://www.kde.org/
Source:         https://download.kde.org/stable/plasma/%{version}/plasma-phone-components-%{version}.tar.xz
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-rpm-macros
BuildRequires:  desktop-file-utils
BuildRequires:  fdupes
BuildRequires:  cmake(KF5I18n) >= %{kf5_version}
BuildRequires:  cmake(KF5KIO) >= %{kf5_version}
BuildRequires:  cmake(KF5ModemManagerQt) >= %{kf5_version}
BuildRequires:  cmake(KF5Notifications) >= %{kf5_version}
BuildRequires:  cmake(KF5Plasma) >= %{kf5_version}
BuildRequires:  cmake(KF5PlasmaQuick) >= %{kf5_version}
BuildRequires:  cmake(KF5Service) >= %{kf5_version}
BuildRequires:  cmake(KF5Wayland) >= %{kf5_version}
BuildRequires:  cmake(KWinDBusInterface)
BuildRequires:  cmake(Qt5Core) >= 5.15.0
BuildRequires:  cmake(Qt5Qml)
BuildRequires:  cmake(Qt5Quick)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(gstreamer-1.0) >= 1.1.90
# Forced on startup
Requires:       qqc2-breeze-style
# QML imports
Requires:       kf5-bluez-qt
Requires:       kf5-kactivities
Requires:       kf5-kdeclarative
Requires:       kf5-kirigami2
Requires:       kwin
Requires:       qt5-qtgraphicaleffects
Requires:       qt5-qtquickcontrols
Requires:       qt5-qtquickcontrols2
Requires:       qt5-qtwayland
Requires:       plasma-milou
Requires:       plasma-nm
Requires:       plasma-nano
Requires:       plasma-pa
Requires:       plasma-workspace >= %{_plasma5_bugfix}

%description
Plasma shell and components targeted for phones.

#%lang_package

%global debug_package %{nil}

%prep
%autosetup -p1 -n plasma-phone-components-%{version}

%build
%cmake_kf5
%cmake_build

%install
%cmake_install
%find_lang %{name} --all-name

# Wut?
sed -i '#touch /tmp/simplelogin_starting#d' %{buildroot}%{_kf5_bindir}/kwinwrapper

%fdupes %{buildroot}

%files -f %{name}.lang
%dir %{_datadir}/wayland-sessions/
%dir %{_kf5_datadir}/plasma/look-and-feel/
%dir %{_kf5_datadir}/plasma/plasmoids/
%dir %{_kf5_datadir}/plasma/quicksettings/
%dir %{_kf5_datadir}/plasma/shells
%dir %{_kf5_qmldir}/org/
%dir %{_kf5_qmldir}/org/kde/
%dir %{_kf5_qmldir}/org/kde/plasma/
%dir %{_kf5_qmldir}/org/kde/plasma/private/
%license LICENSES/*
%{_datadir}/wayland-sessions/plasma-mobile.desktop
%{_kf5_bindir}/kwinwrapper
%{_kf5_datadir}/knotifications5/plasma_phone_components.notifyrc
%{_kf5_datadir}/kservices5/plasma-applet-org.kde.phone.activities.desktop
%{_kf5_datadir}/kservices5/plasma-applet-org.kde.phone.homescreen.desktop
%{_kf5_datadir}/kservices5/plasma-applet-org.kde.phone.krunner.desktop
%{_kf5_datadir}/kservices5/plasma-applet-org.kde.phone.panel.desktop
%{_kf5_datadir}/kservices5/plasma-applet-org.kde.phone.taskpanel.desktop
%{_kf5_datadir}/kservices5/plasma-applet-org.kde.plasma.airplanemode.desktop
%{_kf5_datadir}/kservices5/plasma-applet-org.kde.plasma.nightcolor.desktop
%{_kf5_datadir}/kservices5/plasma-applet-org.kde.plasma.phone.desktop
%{_kf5_datadir}/kservices5/plasma-applet-org.kde.plasma.phoneshell.desktop
%{_kf5_datadir}/plasma/look-and-feel/org.kde.plasma.phone/
%{_kf5_datadir}/plasma/plasmoids/org.kde.phone.activities/
%{_kf5_datadir}/plasma/plasmoids/org.kde.phone.homescreen/
%{_kf5_datadir}/plasma/plasmoids/org.kde.phone.krunner/
%{_kf5_datadir}/plasma/plasmoids/org.kde.phone.panel/
%{_kf5_datadir}/plasma/plasmoids/org.kde.phone.taskpanel/
%{_kf5_datadir}/plasma/quicksettings/org.kde.plasma.airplanemode/
%{_kf5_datadir}/plasma/quicksettings/org.kde.plasma.nightcolor/
%{_kf5_datadir}/plasma/shells/org.kde.plasma.phoneshell/
%{_kf5_metainfodir}/org.kde.phone.activities.appdata.xml
%{_kf5_metainfodir}/org.kde.phone.krunner.appdata.xml
%{_kf5_metainfodir}/org.kde.plasma.phone.appdata.xml
%{_kf5_metainfodir}/org.kde.plasma.phoneshell.appdata.xml
%{_kf5_qmldir}/org/kde/plasma/mm/
%{_kf5_qmldir}/org/kde/plasma/private/mobilehomescreencomponents/
%{_kf5_qmldir}/org/kde/plasma/private/mobileshell/
%{_qt5_plugindir}/plasma/applets/plasma_applet_phonepanel.so
%{_qt5_plugindir}/plasma/applets/plasma_containment_phone_homescreen.so
%{_qt5_plugindir}/plasma/applets/plasma_containment_phone_taskpanel.so

%changelog
* Thu Dec 23 2021 Yoda
- update to 5.23.4
* Tue Oct 12 2021 Yoda
- Initial package
