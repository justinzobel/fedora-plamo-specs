Name:           vnc-screen
Version:        0.3.4
Release:        1%{?dist}
Summary:        Remote desktop application for Kirigami KDE based on VNC standard
License:        GPLv3-or-later
URL:            https://invent.kde.org/marcellodgl/vnc-screen
Source0:        https://invent.kde.org/marcellodgl/vnc-screen/-/archive/%{version}/vnc-screen-%{version}.tar.gz

BuildRequires:  cmake
BuildRequires:  extra-cmake-modules
BuildRequires:  g++

BuildRequires: cmake(Qt5Core)
BuildRequires: cmake(Qt5Quick)
BuildRequires: cmake(Qt5Test)
BuildRequires: cmake(Qt5Gui)
BuildRequires: cmake(Qt5QuickControls2)
BuildRequires: cmake(Qt5Widgets)

BuildRequires: cmake(KF5CoreAddons)
BuildRequires: cmake(KF5I18n)
BuildRequires: cmake(KF5Kirigami2)

BuildRequires: pkgconfig(libvncclient)

%description
%summary.

%prep
%autosetup


%build
%cmake
%cmake_build


%install
%cmake_install


%files
%license COPYING LICENSE
%doc README.md TODO.md
%{_bindir}/vncscreen
%{_datadir}/applications/org.kde.vncscreen.desktop
%{_datadir}/icons/hicolor/scalable/apps/vncscreen.svg
%{_datadir}/locale/it/LC_MESSAGES/vncscreen.mo



%changelog
* Fri Feb 03 2023 Justin Zobel <justin@1707.io> - 0.3.4-1
- Initial Version
